// ****************  *************
// 

$fa=0.5; // default minimum facet angle
$fs=0.5; // default minimum facet size

nvalves = 2; // number of valves
vw   = 10; // width for each one
//vw2  = 12.8; // width of square on front
vwd = 5; // distance in bewteen
vr = 80; 

tr = 5;
tl = 9;

h1 = 5; // height of walls

// == Base
l1 = 20; // length
w1 = nvalves*vw+(nvalves+1)*vwd; // width

w2 = 10; // for screwing stuff

MakeArray1(1);

translate([l1+3,0,0])
MakeArray2(1);

module MakeArray1(textp){
union() {
    difference(){
	union(){
	    // body
	    cube([l1,w1,h1],center=true);
	}
	// 
	for (i = [0:nvalves-1]){
	    translate([0,i*vw+(i-1)*vwd,0]){
		translate([0,-vw/2+tr/2,0]){
		    cylinder(h=h1+2,r=tr,center=true); 
		    translate([-l1/4-1,0,0])
		    cube([l1/2+1,tl,h1+2],center=true);
		}
	    }
	}
	// numbers of valves
	if(textp==1){
	    for (i = [0:nvalves-1]){
		translate([0,(i)*vw+(i)*vwd,0])
		translate([l1/2-3,-w1/2+vwd+vw/2,vwd/2-0.9])
		rotate([0,0,-90])
		linear_extrude(height=1){
		    text(str(i+1),size=3,valign="center",halign="center");
		}
	    }
	}
	// holes on corners
	for (a=[0,90]){
	    for (b=[0,90]){
		mirror([a,0,0])mirror([0,b,0])
		translate([l1/2-h1/2,w1/2-h1/2,vr/2+vwd/2])
		cylinder(h=vr+3*vwd,r=1.6,center=true); 
	    }
	}
	// holes for screws
	for (a=[0,90]){
	    mirror([0,a,0]){
		for (b = [0,180]){
		    rotate([0,b,0]){
			translate([0,(w1+w2)/2,0]){
			    translate([l1/4,0,h1/2])
			    cylinder(h=h1,r=3.5,center=true); 
			    translate([l1/4,0,-1])
			    cylinder(h=h1+2,r=1.6,center=true); 
			}
		    }
		}
	    }
	}
    }
}
}

module MakeArray2(textp){
union() {
    difference(){
	union(){
	    // base
	    cube([l1,w1+2*w2,h1],center=true);
	    // body
	    translate([0,0,vr/2+vwd/2])
	    cube([l1,w1,vr],center=true);
	}
	// square difference for one side
	for (i = [0:nvalves-1]){
	    translate([0,i*vw+(i-1)*vwd,0])
	    translate([0,-w1/2+vw+2*vwd,0])
	    translate([-l1/2+vwd,-vw/2,vr/2+h1])
	    cube([l1,vw,vr],center=true);
	}
	// numbers of valves
	if(textp==1){
	for (a=[0,90]){
	    mirror([a,0,0])
	    for (i = [0:nvalves-1]){
		translate([0,(i)*vw+(i)*vwd,0])
		translate([-l1/2+0.5,-w1/2+vwd+vw/2,vwd/2])
		rotate([90,0,270])
		linear_extrude(height=1){
		    text(str(i+1),size=3,valign="center",halign="center");
		}
	    }
	}
	}
	// holes on corners
	for (a=[0,90]){
	    for (b=[0,90]){
		mirror([a,0,0])mirror([0,b,0])
		translate([l1/2-h1/2,w1/2-h1/2,vr/2+vwd/2])
		cylinder(h=vr+3*vwd,r=1.6,center=true); 
	    }
	}
	// holes for screws
	for (a=[0,90]){
	    mirror([0,a,0]){
		for (b = [0,180]){
		    rotate([0,b,0]){
			translate([0,(w1+w2)/2,0]){
			    translate([l1/4,0,h1/2])
			    cylinder(h=h1,r=3.5,center=true); 
			    translate([l1/4,0,-1])
			    cylinder(h=h1+2,r=1.6,center=true); 
			}
		    }
		}
	    }
	}
    }
}
}