// TEST FOR TUBE HOLDER

$fa=0.2; // default minimum facet angle is now 0.5
$fs=0.2; // default minimum facet size is now 0.5 mm

l1 = 161;
w1 = 110;
h1 = 4;

l2 = 100;
w2 = 70;
h2 = 10;

l3 = 40;
w3 = 20;

l4 = 50;
w4 = 25;
h4 = 0.5;

d5 = 30;
h5 = 3;
w5 = w4+d5;
l5 = w5+10;

r6a = 10;
r6b = 20;

r7 = 2;
d7 = 5;

l8 = 10;
d8 = 18;

rotate([0,0,0]){
    difference(){
	union(){
	    translate([0,0,0])
	    roundedcube(w1,l1,h1,20);
	    
	    translate([0,0,h1])
	    roundedcube(w2,l2,h2,20);
	    // minus some shit
	}
	translate([0,0,-0.1])
	roundedcube(w3,l3,h2+h1+0.2,3);
	
	translate([-w4/2,-l4/2,h1+h2-h4])
	cube([w4,l4,h4+0.1]);
	
	for(a = [0,90]){
	    for(b = [0,90]){
		mirror([0,b,0]){
		    mirror([a,0,0]){
			screwhole1();
		    }
		}
	    }
	}
    }
    
    translate([90,0,-h1-h2])
    difference(){
	union(){
	    difference(){
		translate([0,0,h1+h2])
		roundedcube(w5,l5,h5,10);
		
		translate([0,0,h1+h2-0.1])
		cylinder(h=h5+0.2,r1=r6a,r2=r6b);
	    }
	    for(a = [0,90]){
		for(b = [0,90]){
		    mirror([0,b,0]){
			mirror([a,0,0]){
			    supportcorner();
			}
		    }
		}
	    }
	}
	
	for(a = [0,90]){
	    for(b = [0,90]){
		mirror([0,b,0]){
		    mirror([a,0,0]){
			screwhole1();
		    }
		}
	    }
	}
    }
}

module roundedcube (lw,ll,lh,dc){
    union(){
	difference(){
	    translate([-lw/2,-ll/2,0]){
		cube([lw,ll,lh]);
	    }
	    for(a = [0,90]){
		for(b = [0,90]){
		    mirror([0,b,0]){
			mirror([a,0,0]){
			    translate([-lw/2-1,-ll/2-1,0-0.1]){
				cube([dc+1,dc+1,lh+0.2]);
			    }
			}
		    }
		}
	    }
	}
	for(a = [0,90]){
	    for(b = [0,90]){
		mirror([0,b,0]){
		    mirror([a,0,0]){
			translate([-lw/2+dc,-ll/2+dc,0]){
			    cylinder(h=lh,r=dc);
			}
		    }
		}
	    }
	}
    }
}


module screwhole1 (){
    translate([-w4/2-d7,-l4/2+d7,-0.1])
    cylinder(h=h1+h2+h5+0.2,r=r7);
}

module supportcorner(){
    translate([-d8,-d8,0])
    difference(){
	translate([0,0,h1+h2])
	cube([l8,l8,h5]);
	translate([l8+l8/4,l8/4,h1+h2+h5])
	rotate([0,45,45])
	cube([h5*10,l8*sqrt(2),l8]);
	
    }
}