// TEST FOR TUBE HOLDER

$fa=0.2; // default minimum facet angle is now 0.5
$fs=0.2; // default minimum facet size is now 0.5 mm

h = 80.00; // height in mm
//r2 = 13.4; // radius in mm
r2 = 10.4; // radius in mm
r1 = r2+4; // radius in mm

rs1 = 10.2/2; // Sensor hole radius
rs2 = 8.2/2; // Sensor hole radius
shtip = 9.3;
hs = 25.0; // Sensor hole height (center)
//rs1 = 9.4/2; // Sensor hole radius
//rs2 = 9.4/2; // Sensor hole radius
//shtip = 8;
//hs = 30.0; // Sensor hole height (center)

angles = [0,45,90,135,180,-45,-90,-135]; // holes angles
//angles = [0,45,90,135,180,225,270,315]; // holes angles

rotate([0,0,45])
*difference(){
    union(){
	tube(radius1=r1,radius2=r2,height=h+2);
	for (s = angles){
	    sensorbot(angle=s,height=hs,r=rs1+3,h=8,roff=r2+3.1);
	}
	translate([0,0,0])
	difference(){
	    cylinder(r=r1, h=4);
	    translate([0,0,11])
	    sphere(r=11);
	}
    }
    for (s = angles){
	sensor(angle=s,height=hs,r1=rs1,r2=rs2,h=r2);
    }
}

l1  = 60;
h1  = 5;
dh1 = 20;

l2  = 20;
h2  = 15;

r3  = 2;
d3  = 16;


w4 = 5;
l4 = 6;
d3a = 2.5;

// it would be a lot easier to add simitry to this

difference(){
    union() {
	difference(){
	    translate([-l1/2,-l1/2,-dh1]) // 
	    cube([l1,l1,h1]);

	    translate([-l2/2,-l2/2,-dh1-0.1]) // 
	    cube([l2,l2,h2]);

	    translate([-d3,-d3,-dh1-0.1]) // 
	    cylinder(r=r3, h=h1+0.2);
	    translate([d3,-d3,-dh1-0.1]) // 
	    cylinder(r=r3, h=h1+0.2);
	    translate([-d3,d3,-dh1-0.1]) // 
	    cylinder(r=r3, h=h1+0.2);
	    translate([d3,d3,-dh1-0.1]) // 
	    cylinder(r=r3, h=h1+0.2);
	    
	}
    }
}

*difference(){
    union() {
	difference(){
	    translate([-30,-30,-20]) // 
	    cube([60,60,5]);
	    translate([-10,-10,-25]) // 
	    cube([20,20,15]);
	    translate([-16,-16,-30]) // 
	    cylinder(r=2, h=60);
	    translate([-16,16,-30]) // 
	    cylinder(r=2, h=60);
	    translate([16,16,-30]) // 
	    cylinder(r=2, h=60);
	    translate([16,-16,-30]) // 
	    cylinder(r=2, h=60);
	    translate([-2.5,-31,-35]) // 
	    
	    cube([5,6,50]);
	    translate([-31,-2.5,-35]) // 
	    cube([6,5,50]);
	    
	rotate([0,0,45])
	translate([-8,34.5,-35]) // 
	cube([16,8,50]);
	rotate([0,0,-135])
	translate([-8,34.5,-35]) // 
	cube([16,8,50]);
	rotate([0,0,-45])
	translate([-8,34.5,-35]) // 
	cube([16,8,50]);
	rotate([0,0,135])
	translate([-8,34.5,-35]) // 
	cube([16,8,50]);
    }
    
    //translate([30,-2.5,-30]) // 
    //cube([5,5,35]);
    //translate([-2.5,30,-30]) // 
    //cube([5,5,35]);
    
    difference(){
	translate([-30,-24,5]) // 
	cube([5,18,30]);
	
	rotate([0,90,0])
	translate([-27.5,-13.5,-31]) // 
	cylinder(r=4, h=10);

	rotate([0,0,135])
	translate([-8,34.5,-26]) // 
	cube([16,8,70]);
    }
    
    difference(){
	translate([14,21,5]) // 
	cube([5,9,8]);
	rotate([0,90,0])
	translate([-9,25.5,10]) // 
	cylinder(r=3, h=10);
    }
    difference(){
	translate([21,14,5]) // 
	cube([9,5,8]);
	rotate([90,0,0])
	translate([25.5,9,-20]) // 
	cylinder(r=3, h=10);
    }
    difference(){
	translate([-19,21,5]) // 
	cube([5,9,8]);
	rotate([0,90,0])
	translate([-9,25.5,-20]) // 
	cylinder(r=3, h=10);
    }
    difference(){
	translate([-19,-30,5]) // 
	cube([5,9,8]);
	rotate([0,90,0])
	translate([-9,-25.5,-20]) // 
	cylinder(r=3, h=10);
    }
    difference(){
	translate([-30,14,5]) // 
	cube([9,5,8]);
	rotate([90,0,0])
	translate([-25.5,9,-20]) // 
	cylinder(r=3, h=10);
    }
    difference(){
	translate([14,-30,5]) // 
	cube([5,9,8]);
	rotate([0,90,0])
	translate([-9,-25.5,10]) // 
	cylinder(r=3, h=10);
    }
    difference(){
	translate([21,-19,5]) // 
	cube([9,5,8]);
	rotate([90,0,0])
	translate([25.5,9,10]) // 
	cylinder(r=3, h=10);
    }
    
    difference(){
	
	difference(){
	    translate([-30,-30,-30]) // 
	    #cube([60,60,35]);
	}
	
	//translate([-2.5,-31,-35]) // 
	//cube([5,6,50]);
	//translate([-31,-2.5,-35]) // 
	//cube([6,5,50]);
	
	//translate([10,20,-15]) // 
	//cube([5,15,25]);
	//translate([-20,-35,-15]) // 
	//cube([5,15,25]);
	
	
	rotate([0,0,45])
	translate([-8,34.5,-26]) // 
	cube([16,8,50]);
	rotate([0,0,-135])
	translate([-8,34.5,-26]) // 
	cube([16,8,50]);
	rotate([0,0,-45])
	translate([-8,34.5,-26]) // 
	cube([16,8,50]);
	rotate([0,0,135])
	translate([-8,34.5,-26]) // 
	cube([16,8,50]);
	
	translate([26.8,26.8,-35]) // 
	cylinder(r=2, h=60);
	translate([26.8,-26.8,-35]) // 
	cylinder(r=2, h=60);
	translate([-26.8,26.8,-35]) // 
	cylinder(r=2, h=60);
	translate([-26.8,-26.8,-35]) // 
	cylinder(r=2, h=60);
	
	translate([-21,-21,-45]) // 
	cube([42,42,60]);
	
	rotate([0,0,45]){
	    difference()
	    {
		union() {
		    tube(radius1=r1,radius2=r2,height=h);
		    for (s = angles){
			sensorbot(angle=s,height=hs,r=rs1+3,h=8,roff=r2+3.1);
		    }
		}
		for (s = angles){
		    sensor(angle=s,height=hs,r1=rs1,r2=rs2,h=r2);
		}
	    }
	}
    }
}
translate([-31,-5,-15])
cube([11,10,21]);
}


module tube(radius1=20,radius2=10,height=10) {
    difference(){
	cylinder(r=radius1, h=height);
	translate([0,0,-height*0.1]) // start below the surface
	cylinder(r=radius2, h=height+height*0.2);
    }
    
}

module sensorbot(angle=0,height=10,roff=10,r=3,h=3) {
    width = 2*r;
    union(){
	rotate([90,0,angle])
	translate([0,height,0])
	translate([0,0,roff])
	cylinder(r=r, h=h);
	rotate([90,0,angle])
	translate([-r,0,0])
	translate([0,0,roff-3])
	cube([2*r,hs,h+3]);
    }
}

module sensor(angle=0,height=10,r1=3,r2=2.5,h=10) {
    h1 = h+shtip;
    h2 = h+r1+10;
    rotate([90,0,angle])
    translate([0,height,0])
    cylinder(r=r1, h=h1);
    rotate([90,0,angle])
    translate([0,height,0])
    cylinder(r=r2, h=h2);
}