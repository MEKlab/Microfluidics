3D objects for microfluidics
====================

Description of files
-----------

### SpinCoater2CoverSlip

An adapter for coverslips to be centrifuged in a spin coater. 4 pieces.
This is meant to be used for loading cells in a mother machine.

### HookValves

Adapter for solenoid valves (LFA serie). 2 pieces.

### HookPump

### HookTubes

### HookCircuits

### Stage1

Microscope stage based on JB design. 2 pieces.

### StageI3091

Adapter for coverslips into I-3091. Something in bewteen I-3091 and stage1. 2 pieces.


