Title
====================

File description
---------------------

### 20170517_MM

To make the layers you need to change the layer parameter to layer 0 or 1,
use convert2DXF.pl script to generate the corresponding DXF file.

Chambers on side 1 and 2 are 30 and 25 um respectively.

Width ranges and number of chambers per width.
+    "v1", [0.9, 1, 1.1, 1.2], 450
+    "v2", [1.3, 1.4, 1.5, 1.6], 450
+    "v3", [1.6, 1.7, 1.8, 1.9], 425
+    "vA", [0.9, 1, 1.1, 1.2, 1.3, 1.4, 1.5, 1.6, 1.7, 1.8, 1.9], 150
