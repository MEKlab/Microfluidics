#!/usr/bin/perl

$scad_file = $ARGV[0];

$extra_lines = "  0
SECTION
  2
HEADER
  9
$ACADVER
  1
AC1009
  9
$INSBASE
 10
0.0
 20
0.0
 30
0.0
  9
$EXTMIN
 10
0.0
 20
0.0
 30
0.0
  9
$EXTMAX
 10
0.0
 20
0.0
 30
0.0
  0
ENDSEC
  0
SECTION
  2
TABLES
  0
TABLE
  2
VPORT
 70
0
  0
ENDTAB
  0
TABLE
  2
LTYPE
 70
1
  0
LTYPE
  2
CONTINUOUS
 70
0
  3
Solid line
 72
65
 73
0
 40
0.0
  0
ENDTAB
  0
TABLE
  2
LAYER
 70
1
  0
LAYER
  2
0
 70
64
 62
7
  6
continuous
  0
ENDTAB
  0
TABLE
  2
STYLE
 70
1
  0
STYLE
  2
STANDARD
 70
0
 40
0
 41
1.0
 50
0.0
 71
0
 42
1
 3
ARIAL.TTF
 4

  0
ENDTAB
  0
TABLE
  2
VIEW
 70
0
  0
ENDTAB
  0
ENDSEC";

print("Processing $scad_file\n");

system("openscad -o temp.dxf $scad_file\n");

$outfile = $scad_file;
$outfile =~ s/\.scad/\.dxf/;
print("Saving into $outfile\n");

open(OUT,">$outfile") or die "Can't open $outfile\n";
print OUT "$extra_lines\n";

open(IN,"temp.dxf") or die "Can't open temp.dxf\n";
while(<IN>){
    print OUT $_;
}

close(IN);
close(OUT);

system("rm temp.dxf\n");
