// Let's work with 1um = 1mm for now, and then we can scale it down later.

$fa=0.1; // default minimum facet angle is now 1mm
$fs=0.1; // default minimum facet size is now 1mm
M_PI = 3.14159265358979323846;

// I know I can fit 5 chips in the center, and 3 on each side.

// ======= Mask parameters
layer = 1; // top level control for layers

datestr = "24/05/17";

chipbd = 2500; // distance bewteen chips
nchipsx = 5; //nchipsx = 4; // number of chips in X 
nchipsy = 3; //nchipsy = 2; // number of chips in Y 

// === Chip paremeters for different locations

makemat = [[0,1,1,1,0],[1,1,1,1,1],[0,1,1,1,0]]; // whether to use that location or not
//makemat = [[1,0,0,0,0],[0,0,0,0,0],[0,0,0,0,0]]; // whether to use that location or not
titles = [["t","v1","v1","v1",""],["v2","v2","v2","vA","vA"],["","v3","v3","v3",""]]; // title
// = Chambers
maxws = [[2.0,1.3,1.3,1.3,0],[1.7,1.7,1.7,2.0,2.0],[0,2.0,2.0,2.0,0]]; // max width
minws = [[0.9,0.9,0.9,0.9,0],[1.3,1.3,1.3,0.9,0.9],[0,1.6,1.6,1.6,0]]; // min width
rangeds = [[0.1,0.1,0.1,0.1,0],[0.1,0.1,0.1,0.1,0.1],[0,0.1,0.1,0.1,0]]; // range width distance
nranges = [[150,450,450,450,0],[450,450,450,150,150],[0,425,425,425,0]]; // nrange = 20; // number of chambers per width

// For alignment

ahtri1 = 216; // triangle dim1
ahtri2 = 175; // triangle dim2
adtri = 90;  // triangle spacing
asqr1 = 200; // big square
asqr2 = 34;  // small square
ssqrfz = 50; // font size
nchipt = 2;  // triangles in chip corners
nchipt = 16; // triangles in chip bottom and in between

// ======= Chip parameters
// == Chip border
chipl = 11800; // length of the chip
chipw = 18000; // width of the chip
dchip = 250; // width of the chip border

tfontd = 3000; // distance from border for text
tfontz = 1000; // font size for chip legend

// ======= Flow channels parameters
// Basically each flow channel will cover the region of the chambers,
// plus some extra straight path, then a bended arm and the punch hole.
// All bended arms have the same length.

nflows = 8; // number of flow channels
fl = 250; // flow width
fd = fl+120; // distante to each other
punchr = 400; // radius for puncher

// a bit of extra for the flow before hitting chambers
// text will be printed there
dtotcw = 100; 

armversion = 0;  // arm version
// === for version 0
arml = 3500; //arml = 4000;  // length of the flow arm
darml = 750;  // Delta length 
armlf = 12000; // radius factor (bigger means less bended)

// === for version 1
armminb = 0.88;
armdv1 = 600;

// === text
ftextd = 40; // spacing for flow channel text array
ftextsize = 20; // font size for flow channel text
ftextd2 = ftextsize+5; // spacing bewteen text and channel

// ======= Small features parameters (chamber and cross)
// ==== Chambers
cl1 = 25; // length of up chamber
cl2 = 30; // length of down chamber
cd = 4.5; // distance bewteen chambers

// ==== Text for width ranges
cdtext = cl2+2.5; // distance text from chamber
cfontz = 10; // chamber font size
cwtext2 = 20; // distance of text to each other

// ==== Crosses
lcros   = 10; // cross length
wcros   = 1; // cross width
cdcros  = 10; // distance cross from chamber
cdcros2 = 20; // distance of cross to each other

// === DO NOT TOUCH

cl = cl1+cl2+fl; // length of each chamber in mask
totfd = nflows*fl+(nflows-1)*(fd-fl); 
// Dimensions of chip area
totcl2 = chipl+2*dchip;
totcw2 = chipw+2*dchip;

// Dimensions of waifer area
totcl3 = nchipsx*totcl2+(nchipsx-1)*chipbd;
totcw3 = nchipsy*totcw2+(nchipsy-1)*chipbd;

// when testing with 1 chip:
// totcl3 = totcl2;
// totcw3 = totcw2;

// Reference of mask of 3x3 inches
*difference(){
    circle(r=38100,center=true,$fn=1000);
    // outer circle
    circle(r=38000,center=true,$fn=1000);
}

// a date stamp, assuming mask is 3 inch
for (a = [0,180]){
    rotate([0,0,a])
    translate([33000,0,0]) rotate([0,0,90])
    text(datestr,size=1500,valign="center",halign="center");
}

// Multiple chips
rotate([0,0,-90])
translate([-(totcl3)/2,-(totcw3)/2,0]){
    for (cy = [0:nchipsy-1]){
	// values that change
	maky = makemat[cy];
	tity = titles[cy];
	maxwy = maxws[cy];
	minwy = minws[cy];
	rangedy = rangeds[cy];
	nrangey = nranges[cy];
	for (cx = [0:nchipsx-1]){
	    if(maky[cx]==1){
		// values that change
		title = tity[cx];
		maxw = maxwy[cx];
		minw = minwy[cx];
		ranged = rangedy[cx];
		nrange = nrangey[cx];
		wrange = range([minw:ranged:maxw]);
		echo(title,wrange,nrange);
		// length of flow with chamber
		
		v1 = ntimes(1,len(wrange)-1);
		ctotdw = (nrange*v1)*wrange+(nrange*cd*v1)*v1;
		totcw = ctotdw+dtotcw;
	    
		translate([cx*(totcl2+chipbd),cy*(totcw2+chipbd),0]){
		    // Chip borders
		    if(layer==0){
			difference(){
			    square(size=[totcl2,totcw2]);
			    translate([dchip,dchip,0])
			    square(size=[chipl,chipw]);
			}
			// Some legend for chips
			translate([0,totcw2/2,0]){
			translate([tfontd+dchip,0,0])
			rotate([0,180,90])
			text(str("1"),size=tfontz,valign="center",halign="center");
			translate([totcl2-(tfontd+dchip),0,0])
			rotate([0,180,90])
			text(str("2"),size=tfontz,valign="center",halign="center");
		    }
		    // Chip title in the corners
		    for (a = [0.07,0.93]){
			for (b = [0.07,0.93]){
			    translate([totcl2*a,totcw2*b,0])
			    rotate([0,180,90])
			    text(title,size=tfontz,valign="center",halign="center");
			}
		    }
		}
	    
		// Bottom alignment
		translate([totcl2*0.95,totcw2/2,0])
		drawAligmentArray(layer);
		translate([totcl2*0.05,totcw2/2,0])
		drawAligmentArray(layer);
		
		translate([totcl2*0.5,-chipbd/2,0])
		rotate([0,0,90])
		drawAligmentArray(layer);
		if((cy==nchipsy-1)||(cx==nchipsx-1)||(cx==0)){
		    translate([totcl2*0.5,totcw2+chipbd/2,0])
		    rotate([0,0,90])
		    drawAligmentArray(layer);
		}
		// Actual chip channels
		translate([(totcl2-totfd)/2,(totcw2-(totcw-dtotcw))/2,0])
		for (f=[0:nflows-1]){
		    translate([fd*f,0,0]){
			// Flow
			translate([cl2,-dtotcw/2,0]){
			    if(layer==0){
				union(){
				    // flow on chambers
				    square(size=[fl,totcw]); 
				    // Arms with puncher holes
				    dif = (f+1)-(nflows+1)/2;
				    s = sign(dif);
				    if(armversion==1){
					r = armlf/abs(dif);
					ds = 0;
					if((f+1)%2==0){
					    ds = 1;
					    drawFlowArms_v1(arml,darml,r,ds,s,punchr,totcw);
					}else{
					    ds = -1;
					    drawFlowArms_v1(arml,darml,r,ds,s,punchr,totcw);
					}
				    }
				    if(armversion==0){
					dw = arml*(armminb-2*(abs(dif))/(nflows+1));
					dl = arml-dw;
					for (a=[0,90]){
					    mirror([0,a,0]){
						translate([0,-totcw*a/90,0]){						
						drawFlowArms_v0(s,dw,dl,armdv1,punchr,fl,totcw);
					    }
					}
				    }
				}
			    }
			}
			if(layer==1){
			    // flow text
			    for (a = [fl+ftextd2,-ftextd2]){
				for (b = [totcw-dtotcw/2+ftextd/2,0]){
				    translate([a,b,0])
				    drawTextArray(str("F",f),dtotcw/2,ftextd,0,ftextsize,0);
				}
			    }
			}
		    }
		    
		    // Chambers
		    if(layer==1){
			union(){
			    // Draw channels
			    drawArrayRange(cl,wrange,cd,nrange);
			    // Draw crosses
			    translate([-cdcros,0,0])
			    drawCrossArray(lcros,wcros,ctotdw,cdcros2);
			    translate([cl+cdcros,0,0])
			    drawCrossArray(lcros,wcros,ctotdw,cdcros2);
			    // Draw text
			    drawTextArrays(cl,wrange,cd,nrange,-cdtext,1);
			    if(f==nflows-1){ 
				// the last flow channel won't have any
				// text of width info on one of the
				// side, so we need to add it extra
				drawTextArrays(cl,wrange,cd,nrange,cl+cdtext,1);
			    }
			}
		    }
		}
	    }
	}
    }
}
}
}
module drawAligmentArray(lay){
    // triangles
    
    for (a = [0,90]){
	mirror([0,a,0])
	translate([0,-((nchipt-1)*ahtri1+(nchipt+1)*adtri+asqr1/2),0]){
	    for(n=[0:nchipt-1]){
		translate([0,n*ahtri1+(n-1)*adtri,0]){
		    for(o=[0,90]){
			rotate([0,0,90])
			mirror([0,o,0]) 
			triangle(ahtri1,ahtri2);
		    }
		}
	    }
	}
    }
    // middle square
    translate([-asqr1/2,-asqr1/2,0]){
	if(lay==0){
	    difference(){
		square(size=[asqr1,asqr1]);
		translate([asqr1/2,asqr1/2,0])
		drawCross(30,10);
		translate([asqr1/2,asqr1/4,0])
		text("2-1",size=21,valign="center",halign="center");
	    }
	}else{
	    translate([(asqr1-asqr2)/2,(asqr1-asqr2)/2,0])
	    square(size=[asqr2,asqr2]);
	}
	if(lay==1){
	    translate([asqr1/2,3*asqr1/4,0])
	    rotate([0,0,180])
	    text("2-1",size=21,valign="center",halign="center");
	}
    }
}

module drawFlowArms_v0(s,dw,dl,ar,pr,fl,totcw){
    union(){
	difference(){
	    union(){
		translate([0,totcw,0])
		square(size=[fl,dw]); // straight
		translate([(1-s)*fl/2,totcw+dw+(1+s)*fl/2,0])
		rotate([0,0,-s*90])
		translate([0,0,0])
		square(size=[fl,dl]); // perpendicular
	    }
	    translate([(1-s)*fl/2,totcw+dw-ar+fl,0])
	    rotate([0,0,(1-s)*90/2])
	    square(size=[ar,ar]); // remove square
	}
	// add an arc connecting them
	r = ((ar-fl/2)*sqrt(2)+fl)/2;
	translate([s*r+fl/2,-dw+r-fl/2,0])
	rotate([0,0,s*135])
	2D_arc(fl,r,deg=95,fn=10000);
    }
    translate([s*(dl+fl/2),totcw+dw+fl/2,0])
    translate([fl/2,0,0])
    circle(r=pr,$fn=1000);
}

module drawFlowArms_v1(al,dal,r,ds,s,pr,dw){
    angle1 = (al+ds*dal)*360/(2*M_PI*r);
    translate([s*(r+s*fl/2),0,0])
    rotate([0,0,s*(90+angle1/2)])
    2D_arc(fl,r,deg=angle1,fn=1000);
			    
    d3 = r*2*sin(angle1/2);
    angle3 = 90-asin(r*sin(angle1)/d3);
    translate([s*d3*sin(angle3),-d3*cos(angle3),0])
    translate([fl/2,0,0])
    circle(r=pr,$fn=1000);
    
    angle2 = (al-ds*dal)*360/(2*M_PI*r);
    translate([s*(r+s*fl/2),dw,0])
    rotate([0,0,s*(90-angle2/2)])
    2D_arc(fl,r,deg=angle2,fn=1000);						
    
    d4 = r*2*sin(angle2/2);
    angle4 = 90-asin(r*sin(angle2)/d4);
    translate([s*d4*sin(angle4),dw+d4*cos(angle4),0])
    translate([fl/2,0,0])
    circle(r=pr,center=true,$fn=1000);
}

module drawArrayRange(l,wrange,d,n){
    nw = len(wrange);
    inds = range([0:nw-1]);
    for (wi = inds){
	w = wrange[wi];
	if(wi>0){
	    v1 = ntimes(1,wi-1);
	    v2 = subarray(wrange,end=wi);
	    dw = (n*v1)*v2+((n)*d*v1)*v1;
	    translate([0,dw,0]){
		drawChamberArray(l,w,d,n);
	    }
	}else{
	    drawChamberArray(l,w,d,n);
	}
    }
}
module drawChamberArray(l,w,d,n){
    for (i = [0:n-1]){
	translate([0,i*d+(i-1)*w,0])
	drawChamber(l,w);
    }
}

module drawChamber(l,w){
    square(size=[l,w]);
}

module drawCrossArray(l,w,dt,tr){
    for (tp = [0:tr:dt]){
	translate([0,tp,0])
	drawCross(l,w);
    }
}
module drawCross(l,w){
    rotate([0,0,90])
    translate([-l/2,-w/2,0])
    square(size=[l,w]);
    translate([-l/2,-w/2,0])
    square(size=[l,w]);
}

module drawTextArrays(l,wrange,d,n,dt,drawmark){
    nw = len(wrange);
    inds = range([0:nw-1]);    
    for (wi = inds){
	w = wrange[wi];
	if(wi>0){
	    v1 = ntimes(1,wi-1);
	    v2 = subarray(wrange,end=wi);
	    dw = (n*v1)*v2+((n)*d*v1)*v1;
	    translate([dt,dw,0])
	    drawTextArray(str(wi),n*w+(n-1)*d,cwtext2,90,cfontz,drawmark);
	}else{
	    translate([dt,0,0])
	    drawTextArray(str(wi),n*w+(n-1)*d,cwtext2,90,cfontz,drawmark);
	}
    }
}

module drawTextArray(txt,dt,tr,ori,fsize,drawmark){
    if(drawmark==1){
	translate([-fsize,dt,0])
	square(size=[10,2]);
	translate([fsize-10,dt,0])
	square(size=[10,2]);
    }
    for (tp = [cd:tr:(dt-tr/2)]){
	translate([0,tp,0])
	rotate([0,0,ori])
	text(txt,size=fsize,valign="center",halign="center");
    }
}

function ntimes(x,n) = [ for(i=[0:n]) x ];

// Can't remember where I took it from...
module 2D_arc(w,r,deg=90,fn=100){
    render() {
	difference() {
	    // outer circle
	    circle(r=r+w/2,center=true,$fn=fn);
	    // outer circle
	    circle(r=r-w/2,center=true,$fn=fn);
	    //remove the areas not covered by the arc
	    translate([sin(90-deg/2)*((r+w/2)*2),
		    -sin(deg/2)*((r+w/2)*2)])
	    rotate([0,0,90-deg/2])
	    translate([((r+w/2))-sin(90-(deg)/4)*((r+w/2)),
		    ((r+w/2))*2-sin(90-(deg)/4)*((r+w/2))])
	    square([sin(90-deg/4)*((r+w/2)*2),
		    sin(90-deg/4)*((r+w/2)*2)],center=true);
	    translate([-sin(90-(deg)/2)*((r+w/2)*2),
		    -sin(deg/2)*((r+w/2)*2)])
	    rotate([0,0,-(90-deg/2)])
	    translate([-((r+w/2))+sin(90-(deg)/4)*((r+w/2)),
		    ((r+w/2))*2-sin(90-(deg)/4)*((r+w/2))])
	    square([sin(90-deg/4)*((r+w/2)*2),
		    sin(90-deg/4)*((r+w/2)*2)],center=true);
	}
	//line to see the arc
	//#translate([-((r)*2)/2, sin((180-deg)/2)*(r)]) square([(r+w/2)*2+1,0.01]); 
    }
}

// https://github.com/openscad/MCAD/blob/master/triangles.scad
module triangle(o_len,a_len){
    polygon(points=[[0,0],[a_len,0],[0,o_len]], paths=[[0,1,2]]);
}

// https://github.com/openscad/scad-utils/blob/master/lists.scad
function range(r) = [ for(x=r) x ];

function subarray(list,begin=0,end=-1) = [
    let(end = end < 0 ? len(list) : end)
      for (i = [begin : 1 : end-1])
        list[i]
];
