### Accessories

Info on different accesory elements for running microfluidic
experiments. For example: 3D printed adapters, and circuits for control
of devices.

For control of devices and microscope in MATLAB see the
[MicroscopeControl](https://gitlab.com/MEKlab/MicroscopeControl) project.

### MotherMachines

openSCAD scripts for the making of mother machine masks.
